import os
from setuptools import setup


setup(
    name = "blag",
    version = "0.1.0",
    author = "Igor Mosyagin",
    author_email = "shrimpsizemoose@cramur.me",
    description = ("One more django blag application."),
    license = "WTFPL version 2 (http://sam.zoy.org/wtfpl/)",
    url = "http://bitbucket.org/cra/blag",
    packages=['blag'],
    classifiers=[
        "Development Status :: Alpha",
        "Topic :: Utilities",
        "Intended Audience :: Developers",
        "Framework :: Django",
        "License :: GPL Compatible :: WTFPL",
    ],
)

from blag.models import Article, Category
from django.contrib import admin

def publish(modeladmin, request, queryset):
    queryset.update(is_published=True)
publish.short_description = "Publish selected articles"

def unpublish(modeladmin, request, queryset):
    queryset.update(is_published=False)
unpublish.short_description = "Mark selected articles as draft"

class CategoryInline(admin.ModelAdmin):
    model = Category
    extra = 1

class ArticleAdmin(admin.ModelAdmin):
    list_display = ('slug', 'title', 'author', 'tags', 'date_published', 'is_published')
    actions = [publish, unpublish]
    #inlines = (CategoryInline,)

admin.site.register(Article, ArticleAdmin)
admin.site.register(Category)

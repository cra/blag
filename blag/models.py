# coding: utf-8

from datetime import datetime

from django.contrib.sites.models import Site
from django.contrib.auth.models import User
from django.db import models
from tagging.fields import TagField
from tagging.models import Tag


class Category(models.Model):
    """ Category of an article """
    slug = models.SlugField(max_length=200)
    title = models.CharField(u'title', max_length=250)

    @models.permalink
    def get_absolute_url(self):
        return ('blag-posts-in-category', (), { 'slug': self.slug })

    @models.permalink
    def api_link(self):
        return ('ajax-blag-posts-in-category', (), { 'slug': self.slug })

    def __unicode__(self):
        return u"%s (%s)" % (self.title, self.slug)

    def as_dict(self):
        return {
            'slug': self.slug,
            'title': self.title,
            'url': self.api_link(),
            'web': self.get_absolute_url(),
        }


class Article(models.Model):
    slug = models.SlugField(max_length=200, unique_for_date='date_published')
    title = models.CharField(u'title', max_length=250)
    content = models.TextField(blank=True)
    summary = models.TextField(blank=True)
    is_published = models.BooleanField(default=False)
    date_published = models.DateTimeField(default=datetime.now, blank=True, null=True, db_index=True)
    tags = TagField()
    sites = models.ManyToManyField(Site)
    category = models.ManyToManyField(Category)
    author = models.ForeignKey(User, null=True, blank=True, on_delete=models.SET_NULL)

    # TODO save rendered content in model to speed up loading


    def __unicode__(self):
        return self.slug


    def get_tags(self):
        return Tag.objects.get_for_object(self)


    @models.permalink
    def get_absolute_url(self):
        return ('blag-article-detail', (), { 'year': self.date_published.strftime('%Y'),
                                             'month': self.date_published.strftime('%b').lower(),
                                             'day': self.date_published.strftime('%d'),
                                             'slug': self.slug })


    def as_dict(self):
        return {
            'slug': self.slug,
            'url': self.get_absolute_url(),
            'summary': self.summary,
            'categories': {c.slug:{"title": c.title, "url": c.get_absolute_url()} for c in self.category.all()},
            'tags': [u"%s" % t for t in self.get_tags()],
        }

# coding: utf-8

from django.shortcuts import get_object_or_404, redirect
from annoying.decorators import render_to, ajax_request
from annoying.functions import get_config
from django.core.paginator import Paginator, InvalidPage, EmptyPage

from models import Article, Category

@render_to('blag/index.html')
def index(request):
    return {'articles': Article.objects.all().order_by('-date_published'),
            'categories': Category.objects.all()
            }


@render_to('blag/list.html')
def list(request):
    published_articles = Article.objects.filter(is_published=True)
    return {'articles': published_articles.order_by('-date_published')}


@render_to('blag/display.html')
def get_article(request, slug):
    a = get_object_or_404(Article, slug=slug)
    return {'article': a}


@render_to('blag/list.html')
def posts_in_category(request, slug, page=1):
    c = get_object_or_404(Category, slug=slug)

    posts_per_page = get_config('BLAG_ARTICLES_PER_PAGE', 3)
    paginator = Paginator(c.article_set.all(), posts_per_page)
    try:
        articles = paginator.page(page)
    except (InvalidPage, EmptyPage):
        articles = paginator.page(1)

    return {'articles': articles}


def get_article_datebased(request, year, month, day, slug):
    return redirect('blag-article', slug=slug)


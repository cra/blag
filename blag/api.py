# coding: utf-8

from django.shortcuts import render, get_object_or_404
from annoying.decorators import ajax_request

from models import Article, Category


@ajax_request
def posts(request):
    d = []
    for a in Article.objects.all().order_by('date_published'):
        d += [a.as_dict()]
    return d


@ajax_request
def posts_in_category(request, slug):
    category = get_object_or_404(Category, slug=slug)
    response = [a.as_dict() \
        for a in category.article_set.all().order_by('date_published')]
    return response


@ajax_request
def categories(request):
    return [c.as_dict() for c in Category.objects.all()]

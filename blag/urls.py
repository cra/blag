from django.conf.urls import patterns, url, include

API_VERSION_MAJOR = 1
API_VERSION_MINOR = 1

api_link = lambda l: '^api/v%s.%s/' % (API_VERSION_MAJOR, API_VERSION_MINOR) + l

urlpatterns = patterns('',
    url(r'^$', 'content.views.blag_index', name="blag-index"),
    url(r'^list/$', 'blag.views.list', name="blag-list"),

    url(r'category/(?P<slug>[-\w]+)/$', 'blag.views.posts_in_category',
        name="blag-posts-in-category"),
    url(r'category/(?P<slug>[-\w]+)/(?P<page>[-\w]+)/$', 'blag.views.posts_in_category',
        name="blag-posts-in-category"),

    # jsons
    url(api_link(r'categories/(?P<slug>[-\w]+)/$'), 'blag.api.posts_in_category',
        name="ajax-blag-posts-in-category"),
    url(api_link(r'categories/$'), 'blag.api.categories',
        name="ajax-blag-categories"),
    url(api_link(r'posts/$'), 'blag.api.posts',
        name="ajax-blag-posts"),

    # articles by slug
    url(r'^(?P<year>\d{4})/(?P<month>[-\w]+)/(?P<day>\d+)/(?P<slug>[-\w]+)/$',
        'blag.views.get_article_datebased', 
        name="blag-article-detail"),
    url(r'^(?P<slug>[-\w]+)/$', 'blag.views.get_article', name="blag-article"),
)


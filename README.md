Uncoupling cramur.me blag with it's app for blogging. The name is inspired by [http://xkcd.com/148/](this xkcd comic)

SETUP
=====

Start a django project and add 'blag' to your *installed apps*::

    INSTALLED_APPS = (
      'django.contrib.auth',
      # ...
      'blag',
    )


Next, add the blag urls in your *urls.py*::

    urlpatterns = patterns(
        '',
        ('^blag/', include('blag.urls')),
        # ... 
    )



Avaliable settings::

    'BLAG_ARTICLES_PER_PAGE' -- (default == 3)

amount of articles to display per page in views 'posts by category' and 'posts'


API
===

All Articles have a category, some tags and annotation and an unique slug

api/v1.1/categories/
---------------------
Return all available categories::

    [
      {
        'slug': 'news',
        'title': 'The most important news',
        'url': <corresponding API category url>,
        'web': <corresponding web url for this category>,
      },
      {
        ... 
      },
    ]

Category dict example::

    {
      'slug': 'news',
      'title': 'Ze Awesome News',
      'url': '/blag/api/v1.1/categories/news/',
      'web': '/blag/category/news/',
    },

api/v1.1/categories/<slug>
--------------------------
e.g. api/v1.0/category/news

returns all posts within this category::

    [
      {
        'slug': self.slug,
        'web': <web-url of this article>,
        'summary': self.summary,
        'categories': (all categories of this article in dict {<cat_slug>: {"title": <category_title>, "url": <corresponding API url>),
        'tags': [u"%s" % t for t in self.get_tags()],
      },
      {
        ... // next post
      }
    ]

for example::

    [
      {
        'slug': 'lets-rock',
        'web': '/blag/lets-rock/',
        'summary': 'this is our first blog in this category and on this site. We exlpain some stuff here.',
        'categories': {
              "news": {"title": "Ze Awesome News", "url": '/blag/api/v1.1/categories/news/'}, 
              "rock": {"title": "Rock is dead", "url": '/blag/api/v1.1/categories/rock/'},
        'tags': ["rock", "july", "100% awesome"]
      },
      {
        ... // next post
      }
    ]


api/v1.1/posts/
---------------
All posts with same dict as above, for example::

    [
      {
        'slug': 'lets-rock',
        'web': '/blag/lets-rock/',
        'summary': 'this is our first blog in this category and on this site. We exlpain some stuff here.',
        'categories': {
              "news": {"title": "Ze Awesome News", "url": '/blag/api/v1.1/categories/news/'}, 
              "rock": {"title": "Rock is dead", "url": '/blag/api/v1.1/categories/rock/'},
        'tags': ["rock", "july", "100% awesome"]
      },
      {
        ... // next post
      }
    ]


api/v1.1/posts/<slug>
---------------------
Full post data for given slug
